<img width="350px" align="right" src="https://assets.projectoms.com/image/Icon/big/icon-1024.webp"/>

### All About TomsProject 👋

Hi there, I'm TomsProject and I'm the student of A School.

- 🔭 I’m currently working on [Toms Project](https://projectoms.com)
- 🌱 I’m currently learning C++, PHP, React, Git, School-classes and more
- 💬 Ask me about join [Toms Project](https://projectoms.com), learning Chinese, homework problems (Quanpin, 5·3, ...) and more.
- 📫 How to reach me: E-mail: tom@projectoms.com, [Comment](https://www.projectoms.com/pc/view/Write-Message.html), Weibo [@tomsproject](https://weibo.com/tomsproject), Twitter [@zptmo](https://twitter.com/zptmo) or Telegram [@yestp](https://t.me/yestp)
- ✍️ My blog: [TP Blog](https://blog.projectoms.com)
- ⚡ Fun fact: No teacher teaches me how to program, which means I learned how to do it myself. (Thanks to Google, it helps me a lot.)
- 🌎 Languages I can speak: Chinese (Mandarin) and English.

